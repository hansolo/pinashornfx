package pi;

import eu.hansolo.enzo.lcd.Lcd;
import eu.hansolo.enzo.lcd.LcdBuilder;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.Message;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Properties;


public class Main extends Application {
    // Nashorn
    private static final String PATH_TO_SCRIPT_FILES    = "/home/pi/";    // the path to the script files on your Raspberry Pi
    private static final String INIT_SCRIPT_FILE_NAME   = "lcdinit.js";   // the file that will be used to init the lcd
    private static final String CONFIG_SCRIPT_FILE_NAME = "lcdconfig.js"; // the file that will watched and be used to modify the lcd
    private ScriptEngineManager manager;
    private ScriptEngine        engine;
    private String              script;
    private Invocable           inv;
    private Object              scriptObject;
    private FileWatcher         fileWatcher;
    private Thread              dirWatcherThread;

    // JavaFX Control
    private Lcd                 lcd;

    // PiProperties
    private Properties          piProperties;

    // XMPP configuration
    private String              senderName;
    private String              senderPassword;
    private String              server;
    private String              resource;
    private int                 port;
    private XmppManager         xmppManager;


    // ******************* Initialization *************************************
    @Override public void init() {
        // Init Nashorn
        manager = new ScriptEngineManager();
        engine  = manager.getEngineByName("nashorn");

        // Init lcd control
        lcd = LcdBuilder.create()
                        .prefSize(800, 350)
                        .animationDurationInMs(2000)
                        .thresholdVisible(true)
                        .minMeasuredValueVisible(true)
                        .maxMeasuredValueVisible(true)
                        .lowerRightTextVisible(true)
                        .titleVisible(true)
                        .formerValueVisible(true)
                        .decimals(2)
                        .unitVisible(true)
                        .valueFont(Lcd.LcdFont.LCD)
                        .styleClass(Lcd.STYLE_CLASS_STANDARD_GREEN)
                        .build();

        // Read individual settings for the pi
        piProperties   = readPiProperties();
        senderName     = piProperties.getProperty("sender_name");
        senderPassword = piProperties.getProperty("sender_password");
        server         = piProperties.getProperty("sender_server");
        resource       = piProperties.getProperty("sender_resource");
        port           = Integer.parseInt(piProperties.getProperty("port"));

        // Init xmpp connection
        xmppManager    = new XmppManager(this, server, resource, port);

        initializeXmppConnection();
        initLcd();
        initFileWatcher();
    }

    private void initializeXmppConnection() {
        try {
            xmppManager.init();
            xmppManager.login(senderName, senderPassword);
            xmppManager.setStatus(true, senderName + " is online");
        } catch (XMPPException exception) {
            System.out.println("Error connecting to XMPP: XMPPException " + exception);
        }
    }

    private void initLcd() {
        script = getLcdInitScript();
        try {
            engine.eval(script);
            inv = (Invocable) engine;
            scriptObject = engine.get("obj");
        } catch(ScriptException exception) {}

        if (script.isEmpty()) {
            setLcdDefaults();
        } else {
            try {
                inv.invokeMethod(scriptObject, "initLcd", lcd);
            } catch(ScriptException | NoSuchMethodException exception) {
                setLcdDefaults();
            }
        }
    }
    private String getLcdInitScript() {
        StringBuilder scriptContent = new StringBuilder();
        try {
            Path path          = Paths.get(PATH_TO_SCRIPT_FILES + INIT_SCRIPT_FILE_NAME);
            List<String> lines = Files.readAllLines(path, StandardCharsets.UTF_8);
            lines.forEach(line -> scriptContent.append(line));
            System.out.println("Script: " + INIT_SCRIPT_FILE_NAME + " successfully loaded");
        } catch (IOException  exception) {
            System.out.println("Error loading " + INIT_SCRIPT_FILE_NAME + " file");
        }
        return scriptContent.toString();
    }

    private void initFileWatcher() {
        // Attach file fileWatcher
        if (null != dirWatcherThread && dirWatcherThread.isAlive()) dirWatcherThread.interrupt();
        File fileToWatch = new File(PATH_TO_SCRIPT_FILES + CONFIG_SCRIPT_FILE_NAME);
        fileWatcher = new FileWatcher(fileToWatch);
        fileWatcher.setOnFileModified(watcherEvent -> {
            Platform.runLater(new Runnable() {
                @Override public void run() {
                    applyConfigScript();
                }
            });
        });
        fileWatcher.setOnFileRemoved(watcherEvent -> {
            dirWatcherThread.interrupt();
        });
        dirWatcherThread = new Thread(fileWatcher);
        dirWatcherThread.start();
    }


    // ******************* Methods that answers requests **********************
    public void answerStyleRequest(final String STYLE, final String JID) {
        new Thread(() -> {
            try {
                Platform.runLater(() -> lcd.setStyle(STYLE));
                Message message = new Message();
                message.setBody("Style applied");
                xmppManager.sendMessage(message, JID);
            } catch (XMPPException exception) {}
        }).start();
    }
    public void answerScriptRequest(final String SCRIPT, final String JID) {
        new Thread(() -> {
            boolean success = false;
            try {
                engine.eval(SCRIPT);
                inv          = (Invocable) engine;
                scriptObject = engine.get("obj");
                Platform.runLater(() -> {
                    try {
                        inv.invokeMethod(scriptObject, "configLcd", lcd);
                    } catch (ScriptException | NoSuchMethodException exception) {}
                });
                success = true;
            } catch(ScriptException exception) {
                //Platform.runLater(() -> setLcdDefaults());
            }

            try {
                Message message = new Message();
                message.setBody(success ? "Script executed successfully" : "Error executing script");
                xmppManager.sendMessage(message, JID);
            } catch (XMPPException exception) {}

        }).start();
    }


    // ******************* Methods that just execute requests *****************
    public void rebootRequest() {
        try {
            Runtime run = Runtime.getRuntime();
            run.exec("sudo shutdown -r now");
        } catch (IOException exception) {}
    }
    public void shutdownRequest() {
        try {
            Runtime run = Runtime.getRuntime();
            run.exec("sudo shutdown -h now");
        } catch (IOException exception) {}
    }


    // ******************* Private Methods ************************************
    private Properties readPiProperties() {
        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream("pi.properties"));
            if (properties.isEmpty()) createPiProperties(properties);
        } catch (IOException exception) {
            createPiProperties(properties);
        }
        return properties;
    }
    private void createPiProperties(Properties properties) {
        properties.put("id", "THE ID OF YOUR RASPBERRY PI");
        properties.put("sender_name", "THE NAME OF YOUR RASPBERRY PI");
        properties.put("sender_password", "XMPP PASSWORD OF YOUR RASPBERRY PI");
        properties.put("sender_server", "XMPP SERVER URL");
        properties.put("sender_resource", "XMPP RESOURCE NAME OF YOUR RASPBERRY PI");
        properties.put("port", "XMPP PORT USUALLY 5222");
    }

    private void setLcdDefaults() {
        lcd.setTitle("Temperature");
        lcd.setUnit("°F");
        lcd.setLowerRightText("default");
        lcd.setMinValue(32);
        lcd.setMaxValue(212);
        ObservableList l;
    }

    private void applyConfigScript() {
        script = getLcdConfigScript();
        try {
            engine.eval(script);
            inv          = (Invocable) engine;
            scriptObject = engine.get("obj");
        } catch(ScriptException exception) {}

        if (script.isEmpty()) { return;}

        try {
            inv.invokeMethod(scriptObject, "configLcd", lcd);
        } catch(ScriptException | NoSuchMethodException exception) {
            System.out.println("Error executing lcdconfig.js");
        }
    }
    private String getLcdConfigScript() {
        StringBuilder scriptContent = new StringBuilder();
        try {
            Path path          = Paths.get(PATH_TO_SCRIPT_FILES + CONFIG_SCRIPT_FILE_NAME);
            List<String> lines = Files.readAllLines(path, StandardCharsets.UTF_8);
            lines.forEach(line -> scriptContent.append(line));
            System.out.println("Script: " + INIT_SCRIPT_FILE_NAME + " loaded");
        } catch (IOException  exception) {
            System.out.println("Error loading " + INIT_SCRIPT_FILE_NAME + " file");
        }
        return scriptContent.toString();
    }


    // ******************* Application related ********************************
    @Override public void start(Stage stage) {
        StackPane pane = new StackPane();
        pane.setPadding(new Insets(10, 10, 10, 10));
        pane.getChildren().add(lcd);

        Scene scene = new Scene(pane);

        stage.setScene(scene);
        stage.show();
        stage.setOnCloseRequest(event -> stop());
    }

    @Override public void stop() {
        xmppManager.destroy();
        Platform.exit();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
