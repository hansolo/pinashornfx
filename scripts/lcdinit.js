var Lcd = Packages.eu.hansolo.enzo.lcd.Lcd;

var obj = new Object();

obj.initLcd = function(lcd) {
    lcd.title          = 'Default';
    lcd.unit           = '°F';
    lcd.minValue       = 32;
    lcd.maxValue       = 212;
	lcd.lowerRightText = 'init';
}